const e = require('express');
const router = e.Router();
const multer  = require('multer');
const fs = require('fs');
const path = require('path');
//poniższy kod zapisany został w standardzie ES6, 
//który umożliwia odczyt danych wejściowych z tablic lub obiektów
//jako niezależnych zmiennych
//więcej: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment
//https://stackoverflow.com/questions/41058569/what-is-the-difference-between-const-and-const-in-javascript
const { promisify } = require('util');

//stała przechowuje format zachowywania plików na dysku serwera
//które zostaną przesłane od klienta;
//m. in. mamy ustalone miejsce przechowywania (folder projektu/uploads/)
//oraz jaką nazwę plik ma przyjąć w przypadku zapis (brak właściwości
//filename powoduje nadawanie losowych nazw)
//więcej: https://www.npmjs.com/package/multer
const magazyn = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
})
//dodanie mapy przechowywania (stała magazyn) do tworzonego obiektu
//upload
const upload = multer({ storage: magazyn });
//utworzenie 'Obietnicy'. Jest to jeden z mechanizmów asynchronicznego przetwarzania
//kodu/instrukcji - całych bloków, np. funkcji - z mechanizmem obsługi
//wyjątków; generalnie Promise może zwracać jedno zachowanie jeżeli wykona
//się poprawnie oraz inne, gdy wykona się z błędem. "Zachowanie" w tym
//kontekście to po prostu pojedyncza zmienna, instrukcja lub cała funkcja 
//NAJWAŻNIEJSZE  - wszystko wykonuje się asynchronicznie, tj. po utworzeniu
//obiektu Promise czas jego działania nie jest dla nas znany a tym samym
//domyślnie nie mamy pewności, kiedy z wyniku działania tego obiektu będziemy
//mogli korzystać
//więcej: https://developer.mozilla.org/pl/docs/Web/JavaScript/Referencje/Obiekty/Promise
const readDirPromise = promisify(fs.readdir);
const unlinkPromise = promisify(fs.unlink);
const statPromise = promisify(fs.stat);

/*
	Poniższy zapis to tzw. zapis łańcuchowy kodu, gdzie po kolejnych
	spójnikach (kropki) podajemy kolejne metody obiektu zwróconego
	przez metodę route() obiektu router.
	Kolejne metody pozwalają na obsługę kolejnych zdarzeń jakie mogą
	wystąpić podczas obsługi żądania klienta do naszego serwera
*/
router.route('/')
	//kod poniższej metody wykona się, gdy klient wyśle żądanie GET
	//słowo async przez deklaracją funkcji powoduje jej asynchroniczne 
	//wykonywanie (kod JavaScript nie będzie czekał na jej wynik, tj.
	//zachowanie nieblokowalne - non-blocking function
  .get(async (req, res) => {
	  //słowo await nakazuje parserowi JavaScript poczekać na wynik działania
	  //obiektu, którego domyślnym zachowaniem jest działanie asynchroniczne
	  //tym samym tworzymy kod blokujący (blocking function)
    const files = await readDirPromise('uploads/');
    //poniższy obiekt res utworzy obiekt JSON (w celu odpowiedzi do 
	//klienta) z tablica zawierającą nazwy wszystkich plików w katalogu
	//uploads
	res.json({
      code: 200,
      files
    })
  })
	//ten kody wykona się jeżeli dostaniemy polecenie POST od klienta
	//w skrócie - klient wysłał do nas pliki do załadowania na serwer
  .post(upload.array('files'), (req, res) => {
    //odpowiedź będzie zawierać informację jaki plik(i) został przesłany
	//na serwer
	res.json({
      code: 200,
      files: req.files
    })
  })
	//obsługa polecenie DELETE od klienta; w naszym wypadku ma za zadanie
	//wyrzucić wszystkie pliki jakie dostępne są w katalogu uploads
  .delete(async (req, res) => {
    const files = await readDirPromise('uploads/');
	//bardzie elegancki zapis używanej dotychczas metody forEach
    for(file of files) {
      await unlinkPromise(path.join(process.cwd(), 'uploads/', file))
    }
	//odpowiedź będzie zawierać informację, że wszystkie pliki na serwerze zostały
	//skasowane
    res.json({
      code: 200,
      message: 'Files removed.'
    })
  });
//zachowanie serwera gdy podamy nazwę pliku 
//np. http://localhost:1337/plik_do_skasowania.txt

router.route('/:filename')
//operacja wykonywana gdy otrzymamy polecenie GET
  .get(async (req, res) => {
    try {
      await statPromise(path.join(process.cwd(), 'uploads/', req.params.filename));
      res.sendFile(path.join(process.cwd(), 'uploads/', req.params.filename));
    } catch (error) {
      res.json({
		  //plik nie został znaleziony, zamiast 200 wysyłamy kod HTTP 404
        code: 404,
        message: 'Not Found'
      })
    }
  }) 
  //operacja wykonywana gdy otrzymamy polecenie DELETE
  .delete(async (req, res) => {
    try {
      await unlinkPromise(path.join(process.cwd(), 'uploads/', req.params.filename));
      res.json({
        code: 200,
        message: `${req.params.filename} removed.`
      })
    } catch (error) {
      res.json({
        code: 404,
        message: 'Not Found'
      })
    }
  })
module.exports = router;
