/**
	poniższy kod został pobrany z repozytorium https://gitlab.com/runage
	Licencja MIT
	
	Jak użyć projektu:
	
	- pobieramy i rozpakowujemy go do dowolnego katalogu w naszym systemie
	operacyjnym
	- PONIŻSZE OPERACJE WYKONUJEMY W KONSOLI (cmd, sh, bash czy PowerShell)
	- uruchamiamy instalację zależności; jeżeli instalowaliśmy 
	środowisko node.js to wykonujemy polecenie:
	
	npm i 
	
	w katalogu naszego rozpakowanego projektu. Jeżeli korzystamy
	z wersji wolnej (stand-alone) przechodzimy do katalogu obecnego
	projektu i wydajemy polecenie:
	
	<ścieżka_zawierająca_projekt_nodejs>/npm i 
	
	gdzie <ścieżka_zawierająca_projekt_nodejs> może brzmieć (np.):
	
	C:\nodejs (<- to dla Windows)
	/home/admin/nodejs (<- to dla Linux)
	
	czyli ścieżka w naszym systemie operacyjnym, w której rozpakowaliśmy
	pliki wykonywalne node.js
	
	- projekt uruchamiamy analogicznie do instalacji zależności:
	
	node ./index.js 
	
	lub 
	
	<ścieżka_zawierająca_projekt_nodejs>/node ./index.js
	
	Powinniśmy otrzymać komunikat 
	
	UwU started on http://localhost:1337
	
	Jeżeli takiego komunikatu nie zobaczymy, a zamiast tego otrzyamy 
	komunikat błędu, np: 
	
	Error: listen EADDRINUSE: address already in use 127.0.0.1:1337
	
	oznaczać to będzie, że wybrany przez nas port (niekoniecznie adres)
	jest używany przez inną usługę w systemie i należy zmienić ten
	port w odpowiedniej linijce (podana odpowiednia inormacja w 
	komentarzach poniżej)
*/
//podłączamy moduł express - https://github.com/expressjs/express
const express = require('express');
//podłączamy moduł http - obsługa protokołu (klient-serwer)
const http = require('http');
//moduł obsługi parsowania żądań HTTP (uproszczona forma)
const bodyParser = require('body-parser')
//moduł odpowiedzialny za tworzenie ścieżek dostępowych do zasobów (np. C:\katalog)
const path = require('path');
//podłączenie własnego modułu dla naszego projektu znajdującego się
//w katalogu bieżącym, w podfolderze api 
const api = require('./api');

//tworzymy nowy obiekt aplikacji express - kompleksowa obsługa 
//serwera stron WWW (obsługa żądań i odpowiedzi ze wszystkimi możliwymi
//wyjątkami i zależnościami)
const app = express();
//tworzymy nasz serwer poprzez wywołanie metody crateServer; 
const server = http.createServer(app);
//nakazujemy serwerwi nasłuchiwać informacji na określonym porcie
//(w tym wypadku 1337) oraz na określonym adresie (w tym wypadku
//nazwa domenowa localhost, odpowiadająca adresowi lokalnemu  komputera,
//IPv4 127.0.0.1, IPv6 ::1%128
//można podać wskazany adres sieciowy lub domenę, można też podać adres
//0.0.0.0 (Ipv6 ::%128) - oznaczać to będziem że serwero obsłuży każde
//skierowane do niego żądanie bez względu, z jakiej sieci się ono pojawi
server.listen(1337, 'localhost', () => {
	//funkcja ta wykona się jedynie w przypadku prawidłowego zestawienia
	//serwera; po prostu wyświetli nam komunikat, że serwer pracuje 
	//na określonym porcie określonego adresu
  console.log('UwU started on http://localhost:1337');
})

//wskazujemy naszej aplikacji serwerowej, kiedy ma być realizowana obsługa
//żądań URL przekazanych przez klienta 
//podany parametr extended powoduje, że nasz parser (obsługujący żądania)
//będzie korzystał z domyślnej biblioteki querystring
app.use(bodyParser.urlencoded({ extended: false }))
//parsowanie żądań na application/json
app.use(bodyParser.json())
/*
	Poniżej mamy dodaną serię funkcji obsługujących żądania GET. 
	Dzięki temu użytkownik wpisując odpowiednią sekwencję w 
	adres przeglądarki otrzyma określony zasób sieciowy
	W każdym z wypadków przesłany zostanie stosowny pliki, każdy
	umieszczony w katalogu pages folderu, w którym znajduje się
	niniejszy, opisywany plik (index.js)
*/
//poniższa metoda ma za zadanie obsłużyć żądanie od klienta, gdzie 
//podano adres http://localhost:1337
app.get('/', (req, res) => {
  res.sendFile(path.join(process.cwd(), 'pages/index.html'));
});
//poniższa metoda ma za zadanie obsłużyć żądanie od klienta, gdzie
//podano adres http://localhost:1337/doc
app.get('/doc', (req, res) => {
  res.sendFile(path.join(process.cwd(), 'pages/uwu.html'));
});
//poniższa metoda ma za zadanie obsłużyć żądanie od klienta, gdzie
//podano adres http://localhost:1337/quiz
app.get('/quiz', (req, res) => {
  res.sendFile(path.join(process.cwd(), 'pages/jakis_obrazek.jpg'));
});
//kązdy nowy plik zależny dla naszej witryny musi być oddzielnie dodawany
//w osobnej metodzie get; można to zmienić i ujednolicić - zastanowić się jak
//ZADANIE DOMOWE!
app.get('/css/styl.css', (req, res) => {
  res.sendFile(path.join(process.cwd(), 'pages/css/styl.css'));
});
//wskazujemy aplikacji, by użyła naszego modułu.
app.use('/api', api);
